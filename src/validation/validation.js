import * as Yup from 'yup';

// Validation schema
// Contains validation rules and messages
export const registrationSchema = Yup.object().shape({
    firstName: Yup.string()
        .required('First Name is required'),
    lastName: Yup.string()
        .required('Last Name is required'),
    phoneNumber: Yup.string()
        .min(10, 'Phone is in invalid')
        .required('Phone Number is required'),
    email: Yup.string()
        .email('E-mail is not valid!')
        .required('E-mail is required!'),
    password: Yup.string()
        .min(6, 'Password has to be longer than 6 characters!')
        .required('Password is required!'),
    genre: Yup.string()
        .required('Please select an option'),
    isGoing: Yup.bool()
        .oneOf([true], "Must agree to something")
});

export const loginSchema = Yup.object().shape({
    email: Yup.string()
        .email('E-mail is not valid!')
        .required('E-mail is required!'),
    password: Yup.string()
        .min(6, 'Password has to be longer than 6 characters!')
        .required('Password is required!')
});