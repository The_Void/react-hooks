import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom'


const About =  withRouter(({parentMethod, history}) => {

    useEffect(() => {
        parentMethod();
    }, []);

    return (
        <div className="About container text-center mt-5">
            <h2 onClick={() => {history.push("/signup")}}>
                About
            </h2>
        </div>
    );
})

export default About;