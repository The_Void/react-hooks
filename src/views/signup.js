import React, { useEffect } from 'react';
import Register from '../components/register';

const Signup = ({parentMethod}) => {
    useEffect(() => {
        parentMethod();
    }, []);

    return (
        <div className="Signup mt-5">
            <Register />
        </div>
    );
}

export default Signup;