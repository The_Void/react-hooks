import {ADD_ITEM, RENAME_ITEM} from "../actions/actionTypes";

const initialState = {
    name: 'King'
};

const miscReducer = (state = initialState, action) => {
    switch (action.type) {
        case RENAME_ITEM:
            return {
                ...state,
                name: action.payload
            }
        case ADD_ITEM:
            return {
                ...state,
                address: action.payload
            }
    
        default:
            return state;
    }
}

export default miscReducer;