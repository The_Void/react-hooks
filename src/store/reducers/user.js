import { CREATE_USER } from '../actions/actionTypes';
const initialState = {
    firstName: '',
    lastName: '',
    phoneNumber: '',
    email: '',
    password: '',
    isGoing: '',
    genre: '' 
};
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_USER:
            console.log(action.payload);
            return {
                ...state,
                ...action.payload
            }
    
        default:
            return state;
    }
};
export default userReducer;