import { combineReducers } from "redux";
import miscReducer from './misc';
import userReducer from './user';

export default combineReducers({
    miscReducer,
    userReducer    
});