import {ADD_ITEM, RENAME_ITEM, CREATE_USER} from "./actionTypes";

export const _ADD_ITEM = (val) => ({
    type: ADD_ITEM, 
    payload: val
});

export const _RENAME_ITEM = (val) => ({
    type: RENAME_ITEM, 
    payload: val    
});

export const _CREATE_USER = (val) => ({
    type: CREATE_USER, 
    payload: val    
});