import React, { useRef, forwardRef, useImperativeHandle } from 'react';
import { NavLink } from 'react-router-dom';
import { Formik } from 'formik';
import { loginSchema } from '../validation/validation';
import { useSelector, useDispatch } from 'react-redux';
import { _CREATE_USER } from '../store/actions/actions';
import '../styles/modal.scss';

const Modal = forwardRef((props, ref) => {
    const login = useRef();
    const user = useSelector(state => state);
    // Redux hook that mutates state
    const dispatch = useDispatch();
    // Submit function that dispatches to redux
    const onSubmit = (values, {resetForm}) => {
        alert('clicked')
        setTimeout(() => {
            console.log(values);
            
            resetForm();
        }, 1000);
    }

    // The component instance will be extended
    // with whatever you return from the callback passed
    // as the second argument
    useImperativeHandle(ref, () => ({

        getAlert() {
            login.current.classList.remove('d-none');
        }

    }));

    const closeModal = () => {
        login.current.classList.add('d-none');
    }

    const signupRoute = () => {
        // context.router.history.push('/signup');
    }

    return (
        
            <div className="modals">
                <div ref={login} className="centeredOverlay d-none">                           
                    <div className="modal-content modal-dialog modal-lg existing-user ml-5 mr-5">
                        <div className="modal-header justify-content-center">
                            <h5 className="modal-title">
                                Log In
                            </h5>
                            <button type="button" 
                                    className="close pl-0" 
                                    onClick={closeModal} 
                                    aria-label="Close"
                            >
                                <span aria-hidden="true">
                                    ×
                                </span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <Formik
                                initialValues={{
                                    email: '',
                                    password: '',
                                }}
                                validationSchema={loginSchema}
                                onSubmit={onSubmit}
                                user={user}
                            >   
                                {({
                                    values,
                                    errors,
                                    touched,
                                    handleSubmit,
                                    handleChange,
                                    handleBlur,
                                    
                                }) => (
                                    <form onSubmit={handleSubmit} className="container">
                                        {/* Email input */}
                                        <div className="form-group row">
                                            <div className="col-12">
                                                <input  onBlur={handleBlur}
                                                        onChange={handleChange}
                                                        value={values.email}
                                                        name="email"
                                                        type="email"
                                                        className="form-control"
                                                        placeholder="Email"
                                                />
                                                {/* Error message */}
                                                {errors.email && touched.email &&
                                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                                        {errors.email}
                                                    </span>
                                                }
                                            </div>
                                        </div>
                                        {/* Password input */}
                                        <div className="form-group row">
                                            <div className="col-12">
                                                <input  onBlur={handleBlur}
                                                        onChange={handleChange}
                                                        value={values.password}
                                                        name="password"
                                                        type="password"
                                                        className="form-control"
                                                        placeholder="Password"
                                                />
                                                {/* Error message */}
                                                {errors.password && touched.password &&
                                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                                        {errors.password}
                                                    </span>
                                                }
                                            </div>
                                        </div>
                                        {/* Submit button */}
                                        <button type="submit" className="btn float-right">
                                            Submit
                                        </button>
                                    </form>
                                )}
                            </Formik>
                        </div>
                        <div className="modal-footer row">
                            <p className="col">
                                <span className="mr-2">Don't have an account?</span> 
                                <NavLink onClick={closeModal} to={'/signup'}>  
                                    Sign Up
                                </NavLink>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        
    );
});

export default Modal;