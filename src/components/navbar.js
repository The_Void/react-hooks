import React, { useEffect, useRef, useState }  from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink, Link } from 'react-router-dom';
import Home from '../views/home';
import About from '../views/about';
import Contact from '../views/contact';
import Modal from '../components/modal';
import Signup from '../views/signup'
import '../styles/navbar.scss';

export const Navbar = () => {
    const mainNav = useRef();
    const navList = useRef();
    const navButton = useRef();
    const childRef = useRef();

    const addClass = () => {      
        let newMainNav = mainNav.current.classList;
        let newNavList = navList.current.classList;

        newMainNav.contains('main-nav') ? newMainNav.remove('main-nav') : newMainNav.add('main-nav');
        newNavList.contains('custom-nav') ? newNavList.remove('custom-nav') : newNavList.add('custom-nav');
    }

    const closeNavBar = () => {
        let newNavList = navList.current.classList;
        let newMainNav = mainNav.current.classList;
        let newNavButton = navButton.current.classList;    

        newNavList.remove('show');
        newMainNav.remove('main-nav');
        newNavButton.add('collapsed');

        if(window.innerWidth < 992) {
            newNavList.remove('custom-nav');
        }
    }
    
    window.onresize = () => {
        let newNavList = navList.current.classList;
        let newMainNav = mainNav.current.classList;
        let newNavButton = navButton.current.classList;

        if(window.innerWidth >= 992) {
            newNavList.remove('show');
            newMainNav.remove('main-nav');
            newNavButton.remove('collapsed');
            newNavList.remove('custom-nav');
        }

        if(window.innerWidth < 992) {
            newNavButton.add('collapsed');
        }
    }

    return (
        <Router>
            <Modal ref={childRef} />
            <div  id="nav" 
                  className="pl-0 pr-0 sticky-top"
                  ref={mainNav}
            >      
                <nav className="container navbar navbar-expand-lg">
                    <Link className="navbar-brand" to={'/'}> 
                        MVVT
                    </Link>

                    <button ref={navButton}
                            onClick={addClass} 
                            className="navbar-toggler collapsed" 
                            type="button" 
                            data-toggle="collapse" 
                            data-target="#navbarTogglerDemo02" 
                            aria-controls="navbarTogglerDemo02" 
                            aria-expanded="false" 
                            aria-label="Toggle navigation"
                    >
                        <span className="icon-bar top-bar" />
                        <span className="icon-bar middle-bar" />
                        <span className="icon-bar bottom-bar" />
                    </button>

                    <div ref={navList} 
                         className="navbar-collapse collapse" 
                         id="navbarTogglerDemo02"
                    >
                        <ul className="navbar-nav mt-2 mt-lg-0">
                            <li className="nav-item nav-link">
                                <NavLink exact activeClassName="active" to={'/'}> 
                                    Home
                                </NavLink>  
                            </li>
                            <li className="nav-item nav-link">
                                <NavLink activeClassName="active" to={'/about'}>    
                                    About
                                </NavLink>
                            </li>
                            <li className="nav-item nav-link">
                                <NavLink activeClassName="active" to={'/contact'}>  
                                    Contact
                                </NavLink>
                            </li>
                            {false ? (
                                <li className="nav-item nav-link">  
                                    Log Out
                                </li>
                            ) : (
                                <li className="nav-item nav-link" onClick={() => childRef.current.getAlert()}>  
                                    Log In
                                </li>
                            )}
                            
                        </ul>
                    </div>
                </nav>
            </div>
            <Switch>
                <Route exact 
                       path='/' 
                       render={()=> <Home parentMethod={closeNavBar} />} 
                />
                <Route exact 
                       path='/about'
                       render={()=> <About parentMethod={closeNavBar} />}  
                />
                <Route exact 
                       path='/contact' 
                       render={() => <Contact parentMethod={closeNavBar} />} 
                />
                <Route path="/signup" render={() => <Signup parentMethod={closeNavBar} />} />
            </Switch>    
        </Router>
    );
}