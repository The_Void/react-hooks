import React from 'react';
import { useDispatch } from 'react-redux';
import { _CREATE_USER } from '../store/actions/actions';
import useForm from 'react-hook-form';

export const FormExample = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, watch, errors } = useForm({
        mode: 'onBlur'
    });
    const onSubmit = data => { 
        console.log(data);
        dispatch(_CREATE_USER(data)); 
    };

    console.log(watch('example')) // watch input value by passing the name of it

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input name="example" 
                   defaultValue="test" 
                   ref={register} 
            />

            <input name="exampleRequired"
                   ref={register({ required: true })}
            />
            {errors.exampleRequired && <span>This field is required</span>}
            
            <input type="submit" />
        </form>
    )
}