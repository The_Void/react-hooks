import React from 'react';
import { Formik } from 'formik';
import { registrationSchema } from '../validation/validation';
import NumberFormat from 'react-number-format';
import { useSelector, useDispatch } from 'react-redux';
import { _CREATE_USER } from '../store/actions/actions';
import '../styles/form.scss'


// Functional component
const Register = () => {
    const user = useSelector(state => state);
    // Redux hook that mutates state
    const dispatch = useDispatch();
    // Submit function that dispatches to redux
    const onSubmit = (values, {resetForm, setSubmitting}) => {
        setTimeout(() => {
            console.log(values);
            dispatch(_CREATE_USER(values));
            resetForm();
            setSubmitting(false);
        }, 1000);
    }
    const options = [
        { 
            value: 'blues', 
            label: 'Blues' 
        },
        { 
            value: 'rock', 
            label: 'Rock' 
        },
        { 
            value: 'jazz', 
            label: 'Jazz' 
        },
        { 
            value: 'orchestra', 
            label: 'Orchestra' 
        }
    ];
    const list = options.map((op) =>
        <option value={op.value} key={op.value}>
            {op.label}
        </option>
    );
    
    return (
        <Formik
            initialValues={{
                firstName: '',
                lastName: '',
                phoneNumber: '',
                email: '',
                password: '',
                isGoing: false,
                genre: ''
            }}
            validationSchema={registrationSchema}
            onSubmit={onSubmit}
            user={user}
        >   
            {({
                values,
                errors,
                touched,
                handleSubmit,
                handleChange,
                handleBlur,
                handleReset,
                dirty,
                isSubmitting,
            }) => (
                    <form onSubmit={handleSubmit} className="container">
                        {/* Header */}
                        <h1 className="text-center mb-4">
                            Create Acount
                        </h1>
                        {/* First name input */}
                        <div className="form-group row">
                            <div className="col-12">
                                <input  onBlur={handleBlur} 
                                        onChange={handleChange}
                                        value={values.firstName}
                                        name="firstName"
                                        type="text"
                                        className="form-control"
                                        placeholder="First name"
                                />
                                {/* Error message */}
                                {errors.firstName && touched.firstName &&
                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                        {errors.firstName}
                                    </span>
                                }
                            </div>
                        </div>
                        {/* Last name input */}
                        <div className="form-group row">
                            <div className="col-12">
                                <input  onBlur={handleBlur} 
                                        onChange={handleChange}
                                        value={values.lastName}
                                        name="lastName"
                                        type="text"
                                        className="form-control"
                                        placeholder="Last name"
                                />
                                {/* Error message */}
                                {errors.lastName && touched.lastName &&
                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                        {errors.lastName}
                                    </span>
                                }
                            </div>
                        </div>
                        {/* Phone number input */}
                        <div className="form-group row">
                            <div className="col-12">
                                <NumberFormat   onBlur={handleBlur} 
                                                onChange={handleChange}
                                                value={values.phoneNumber}
                                                name="phoneNumber"
                                                displayType={'tel'}
                                                className="form-control"
                                                placeholder="Phone Number"
                                                format="(###) ###-####"
                                                mask="_"
                                />
                                {/* Error message */}
                                {errors.phoneNumber && touched.phoneNumber &&
                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                        {errors.phoneNumber}
                                    </span>
                                }
                            </div>
                        </div>
                        {/* Email input */}
                        <div className="form-group row">
                            <div className="col-12">
                                <input  onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                        name="email"
                                        type="email"
                                        className="form-control"
                                        placeholder="Email"
                                />
                                {/* Error message */}
                                {errors.email && touched.email &&
                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                        {errors.email}
                                    </span>
                                }
                            </div>
                        </div>
                        {/* Password input */}
                        <div className="form-group row">
                            <div className="col-12">
                                <input  onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password}
                                        name="password"
                                        type="password"
                                        className="form-control"
                                        placeholder="Password"
                                />
                                {/* Error message */}
                                {errors.password && touched.password &&
                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                        {errors.password}
                                    </span>
                                }
                            </div>
                        </div>
                        {/* Select options */}
                        <div className="form-group row">
                            <div className="col-12">
                                <select className="custom-select" 
                                        name="genre"
                                        value={values.genre}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                >
                                <option value=''>
                                    Select a genre
                                </option>
                                    {list}
                                </select>
                                {/* Error message */}
                                {errors.genre && touched.genre &&
                                    <span style={{ color: "red", fontWeight: "bold" }}>
                                        {errors.genre}
                                    </span>
                                }
                            </div>
                        </div>
                        {/* Checkbox */}
                        <div className="custom-control custom-checkbox">
                            <input  name="isGoing" 
                                    type="checkbox"
                                    id="customCheck1"
                                    className="custom-control-input"
                                    checked={values.isGoing}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                            />
                            <label className="custom-control-label" htmlFor="customCheck1">
                                I agree to the Terms and Conditions
                            </label>
                        </div>
                        {/* Error message */}
                        {errors.isGoing && touched.isGoing &&
                            <span style={{ color: "red", fontWeight: "bold" }}>
                                {errors.isGoing}
                            </span>
                        }
                        <br />
                        {/* Reset button */}
                        <button type="button"
                                className="btn mt-3"
                                onClick={handleReset}
                                disabled={!dirty || isSubmitting}
                        >
                            Reset
                        </button>
                        {/* Submit button */}
                        <button type="submit" className="btn mt-3 float-right">
                            Submit
                        </button>
                    </form>
                )}
        </Formik>
    )
}

export default Register;